<?php require_once('libs/loader.php'); ?>
<?php include('libs/header.php'); ?>

<div id='renderingCanvas' style="position: relative; top: -20px; left: 0px;"></div>

<div  style="position: relative; top: -20px;"  class="container" id="mainContainer">

	<audio style="display: none; " src='pogo.wav' id="pogoplayer"></audio>
	<div id="diagram-container" class="row">
		<center><button class="btn" id="robotSizeToggleLink" onclick="robotSizeToggle();" style="position: relative; top:-20px;">&or; expand</button></center>
		<br><br>
		<div class="col-lg-11">
			<div id="slider"></div><br>
		</div>
		<div class="col-lg-1" style="">
			<button id="pauseBtn" onclick="playPauseAnimation();" style="margin-top: -10px;" class="btn">pause</button>
		</div>
		<!--
		<center><a href="#" onclick="robotSizeToggle();" id="robotSizeToggleLink" >&or; ausklappen</href></center><br><br>
		-->
	</div>
	<hr>
	<div id="diagram-container" class="row">
		<div class="col-lg-2">
			<label for="x-dimension">X Dimension</label>
			<select class="form-control" name="x-dimension" placeholder="">
				<option value="TimeStamp">Time</option>
				<option value="Energy_Act">Energy current</option>
				<option value="Alarm_Stop">Alarm Stop</option>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="AxisAct.E6AXIS.A'.$i.'">Axis-'.$i.' position</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="AxisActMeas.E6AXIS.A'.$i.'">Measured axis-'.$i.' position</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Fol_Error_A'.$i.'">Axis-'.$i.' setpoint error</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Torque_Axis_Act_A'.$i.'">Axis-'.$i.' Torque</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Torque_Diff_A'.$i.'">Axis-'.$i.' actual-target torque difference</option>'?>
				<?php endfor; ?>
			</select>
		</div>
		<div class="col-lg-2">
			<label for="y-dimension">Y Dimension</label>
			<select class="form-control" name="y-dimension" placeholder="">
				<option value="TimeStamp">Time</option>
				<option value="Energy_Act">Energy current</option>
				<option value="Alarm_Stop">Alarm Stop</option>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="AxisAct.E6AXIS.A'.$i.'">Axis-'.$i.' position</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="AxisActMeas.E6AXIS.A'.$i.'">Measured axis-'.$i.' position</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Fol_Error_A'.$i.'">Axis-'.$i.' setpoint error</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Torque_Axis_Act_A'.$i.'">Axis-'.$i.' Torque</option>'?>
				<?php endfor; ?>
				<?php for($i = 1; $i <= 6; $i++): ?>
				<?php echo '<option value="Torque_Diff_A'.$i.'">Axis-'.$i.' actual-target torque difference</option>'?>
				<?php endfor; ?>
			</select>
		</div>
		<div class="col-lg-2">
			<label for="from">From</label>
			 <input type="text" class="form-control" name="from" value="1374550911895">
		</div>
		<div class="col-lg-2">
			<label for="to">To</label>
			<input type="text" class="form-control" name="to" value="1584550911895">
		</div>

		<div class="col-lg-2">
			<br><button id="add-diagram" style="margin-top: 5px;" class="btn">add diagram</button>
		</div>
	</div><!-- .row -->
	<div class="row">
		<div class="col-md-3">
			
		</div>
	</div>

	<!-- EventLogs select menu --> 
	<hr>

	<div id="chart-container" class="row grid">
	</div><!-- .row -->

	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3>Event Log: Smart HMI</h3>
				<div class="panel-group" id="accordionHMI">
		   			 
		 <?php
		$query = 'SELECT c.EventLogsSmartHMI, c.Type FROM c WHERE c.EventLogsSmartHMI != null';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
		
		if (is_array($result)) {
			for($i = 0; $i < count($result) AND $i < 5; ++$i) {
				$entry = $result[$i]->EventLogsSmartHMI->EventLogEntry;
				$entry = (array)$entry;
				if (isset($entry['@Type'])) {
					switch($entry['@Type']) {
						case 'Information':
						?>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionHMI" href="#collapseHMI<?php echo $i; ?>">Information</a>
							</h4>
						</div>
						<div id="collapseHMI<?php echo $i; ?>" class="panel-collapse collapse">
							 <div class="panel-body">
								 <div class="alert alert-info" role="alert"><b>Type:</b> "Information",
									 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
									 <br><b>Source:</b> <?php echo $entry['Source']; ?>
									 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
								 </div>
							 </div>
						</div>
						</div>
						<?php
						break;
						
						case 'Warning':
						?>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionHMI" href="#collapseHMI<?php echo $i; ?>">Warning</a>
							</h4>
						</div>
						<div id="collapseHMI<?php echo $i; ?>" class="panel-collapse collapse">
							 <div class="panel-body">
								 <div class="alert alert-warning" role="alert"><b>Type:</b> "Warning",
									 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
									 <br><b>Source:</b> <?php echo $entry['Source']; ?>
									 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
								 </div>
							 </div>
						</div>
						</div>
						<?php
						break;
						
						case 'Error':
						default:
						?>
						<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionHMI" href="#collapseHMI<?php echo $i; ?>">Error</a>
							</h4>
						</div>
						<div id="collapseHMI<?php echo $i; ?>" class="panel-collapse collapse">
							 <div class="panel-body">
								 <div class="alert alert-danger" role="alert"><b>Type:</b> "Error",
									 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
									 <br><b>Source:</b> <?php echo $entry['Source']; ?>
									 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
								 </div>
							 </div>
						</div>
						</div>
						<?php
						break;
					}
				
				}
			}
		}
		?>

		  		</div>	
		  	</div>	
			<div class="col-lg-6">
				<h3>Event Log: System</h3>
				<div class="panel-group" id="accordionSystem">
		   			 
					 <?php
		$query = 'SELECT c.EventLogsSystem, c.Type FROM c WHERE c.EventLogsSystem != null';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (!empty($result)) {
			if (isset($result->Documents)) {
				$result = $result->Documents;
			}
			
			if (is_array($result)) {
				for($i = 0; $i < count($result) AND $i < 5; ++$i) {
					$entry = $result[$i]->EventLogsSystem->EventLogEntry;
					
					if (is_array($entry)) {
						$entry = $entry[0];
					}
					
					$entry = (array)$entry;
					if (isset($entry['@Type'])) {
						switch($entry['@Type']) {
							case 'Information':
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionSystem" href="#collapseSystem<?php echo $i; ?>">Information</a>
								</h4>
							</div>
							<div id="collapseSystem<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-info" role="alert"><b>Type:</b> "Information",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
							
							case 'Warning':
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionSystem" href="#collapseSystem<?php echo $i; ?>">Warning</a>
								</h4>
							</div>
							<div id="collapseSystem<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-warning" role="alert"><b>Type:</b> "Warning",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
							
							case 'Error':
							default:
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionSystem" href="#collapseSystem<?php echo $i; ?>">Error</a>
								</h4>
							</div>
							<div id="collapseSystem<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-danger" role="alert"><b>Type:</b> "Error",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
						}
					
					}
				}
			}
		}
		?>

		  		</div>	
		  	</div>	
			
			<div class="col-lg-4">
				<div class="panel-group" id="accordionProgram">
		   			 
					 <?php
					 /*
		$query = 'SELECT c.EventLogsProgram, c.Type FROM c WHERE c.EventLogsProgram != null';
		$jsonDB = $col->query($query);
		if (!empty($jsonDB)) {
			$result = json_decode($jsonDB);
			if (isset($result->Documents)) {
				$result = $result->Documents;
			}
			
			if (is_array($result)) {
				for($i = 0; $i < count($result) AND $i < 5; ++$i) {
					$entry = $result[$i]->EventLogsProgram->EventLogEntry;
					
					if (is_array($entry)) {
						$entry = $entry[0];
					}
					
					$entry = (array)$entry;
					if (isset($entry['@Type'])) {
						switch($entry['@Type']) {
							case 'Information':
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionProgram" href="#collapseProgram<?php echo $i; ?>">Information</a>
								</h4>
							</div>
							<div id="collapseProgram<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-info" role="alert"><b>Type:</b> "Information",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
							
							case 'Warning':
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionProgram" href="#collapseProgram<?php echo $i; ?>">Warning</a>
								</h4>
							</div>
							<div id="collapseProgram<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-warning" role="alert"><b>Type:</b> "Warning",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
							
							case 'Error':
							default:
							?>
							<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a onclick="switchError();" data-toggle="collapse" data-parent="#accordionProgram" href="#collapseProgram<?php echo $i; ?>">Error</a>
								</h4>
							</div>
							<div id="collapseProgram<?php echo $i; ?>" class="panel-collapse collapse">
								 <div class="panel-body">
									 <div class="alert alert-danger" role="alert"><b>Type:</b> "Error",
										 <br><b>DateTime:</b> <?php echo $entry['DateTime']; ?>
										 <br><b>Source:</b> <?php echo $entry['Source']; ?>
										 <br><b>Data:</b> "<?php echo $entry['Data']; ?>"
									 </div>
								 </div>
							</div>
							</div>
							<?php
							break;
						}
					
					}
					
				}
			}
		}
		*/
		?>

		  		</div>	
		  	</div>
		
		</div>	

	</div>

</div>
	
<?php include('libs/footer.php'); ?>