<?php require_once('libs/loader.php');



switch($_GET['type']) {
	case 'chart':
		chart($col);
		break;
	case 'EventLogsSmartHMI':
		$query = 'SELECT c.EventLogsSmartHMI, c.Type FROM c WHERE c.EventLogsSmartHMI != null';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
	
		/*
		echo '<pre>';
		var_dump($result);
		*/
		echo json_encode($result);
		break;

	case 'EventLogsSystem':
		$query = 'SELECT c.EventLogsUser FROM c WHERE c.EventLogsUser != null ';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
	
		/*
		echo '<pre>';
		var_dump($result);
		*/
		echo json_encode($result);
		break;

	case 'EventLogsUser':
		$query = 'SELECT c.EventLogsUser FROM c WHERE c.EventLogsUser != null';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
	
		/*
		echo '<pre>';
		var_dump($result);
		*/
		echo json_encode($result);
		break;

	case 'EventLogsProgram':
		$query = 'SELECT c.EventLogsProgram FROM c WHERE c.EventLogsProgram != null';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
	
		/*
		echo '<pre>';
		var_dump($result);
		*/
		echo json_encode($result);
		break;
	case 'moveset':
		$query = 'SELECT c.AxisAct.E6AXIS FROM c WHERE c.AxisAct.E6AXIS != null AND c.TimeStamp > "' . $_GET['from'] . '" AND c.TimeStamp < "' . $_GET['to'] . '" ORDER BY c.TimeStamp ASC';
		$jsonDB = $col->query($query);
		$result = json_decode($jsonDB);
		if (isset($result->Documents)) {
			$result = $result->Documents;
		}
		
		/*
		echo '<pre>';
		var_dump($result);
		*/
		echo json_encode($result);
		break;
}


function chart($col) {
	$query = 'SELECT c.' . $_GET['xdim'] . ' AS x, c.' . $_GET['ydim'] . ' as y, "' . $_GET['r'] . '" as r FROM c WHERE c.' . $_GET['xdim'] . ' != null AND c.' . $_GET['ydim'] . ' != null AND c.TimeStamp > "' . $_GET['from'] . '" AND c.TimeStamp < "' . $_GET['to'] . '" ORDER BY c.TimeStamp DESC';
	
	$jsonDB = $col->query($query);
	$result = json_decode($jsonDB);
	if (isset($result->Documents)) {
		$result = $result->Documents;
	}
	
	/*
	echo '<pre>';
	var_dump($result);
	*/
	echo json_encode($result);
}








?>