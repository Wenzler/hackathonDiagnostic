$(document).ready(function() {
	Chart.defaults.global.legend.display = false;

	$('.chart').each(function(index) {
		var myChart = new Chart(this, {
			type: 'line',
			data: {
				datasets: [{
					label: 'Scatter Dataset',
					data: [{
						x: -10,
						y: 0
					}, {
						x: 0,
						y: 10
					}, {
						x: 10,
						y: 5
					}],
				}]
			},
			options: {
				 options: {
			        elements: {
			            points: {
			                borderWidth: 2,
			                borderColor: 'rgb(0, 0, 0)'
			            }
			        }
			    }
			}
		});
	});

	

	$('#add-diagram').click(function() {
		var xdim = $('select[name="x-dimension"]').val();
		var xdimTitle = $('select[name="x-dimension"]').find(":selected").text();
		var ydim = $('select[name="y-dimension"]').val();
		var ydimTitle = $('select[name="y-dimension"]').find(":selected").text();
		var from = $('input[name="from"]').val();
		var to = $('input[name="to"]').val();
		createDiagram(xdim, xdimTitle, ydim, ydimTitle, from, to);
	});


});


function createDiagram(xdim, xdimTitle, ydim, ydimTitle, from, to) {
	$.ajax({
		method: "GET",
		url: "api.php",
		data: {
			type: "chart",
			xdim: xdim,
			ydim: ydim,
			from: from,
			to: to,
			r: 3
		}
	}).done(function(response) {
		var newDiagram = 
			'<div class="col-lg-4 tile well"><div class="diagram-title"><strong>x-Dimension:</strong> ' + xdimTitle + '<br><strong>y-Dimension:</strong> ' + ydimTitle + '<div class="pull-right"><a href="#" class="remove-chart">remove</a></div></div><canvas class="chart" width="200" height="200"></canvas></div>';


		$('#chart-container').append(newDiagram);

		var data = {
    		datasets: [
	        {
	            label: 'First Dataset',
	            data: JSON.parse(response)
	        }]
		};

		$('#chart-container')
			.find('div')
			.last()
			.parent()
			.find('.remove-chart')
			//.first()
			.click(function() {
				$(this).parent().parent().parent().remove();
			});

		$('#chart-container').find('.chart').last().each(function() {
			var myChart = new Chart(this, {
				type: 'bubble',
				data: {
					datasets: [
					{
						label: 'First Dataset',
						data: JSON.parse(response),
						backgroundColor:"#FF6384",
        				hoverBackgroundColor: "#FF6384",
        				borderWidth: 2
					}]
				}
			});
		});

		$(".grid").sortable({
			connectWith: '.grid',
			tolerance: 'pointer',
			revert: 'invalid',
			placeholder: 'col-lg-4 well placeholder tile',
			forceHelperSize: true
		});
	});
}
