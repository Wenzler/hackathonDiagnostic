var playbackHistory;
var curFrame = 0;
var playing = true;

function playPauseAnimation(){
	if(playing){
		document.getElementById("pauseBtn").innerHTML = "play";
	}else{
		document.getElementById("pauseBtn").innerHTML = "pause";
	}
	playing = !playing;
}

$(document).ready(function() {
	$( function() {
		$( "#slider" ).slider({
			slide: function( event, ui ) {
				curFrame = ui.value;
				axisRotation[0] = playbackHistory[curFrame].E6AXIS.A1*3.14/180.0;
				axisRotation[1] = playbackHistory[curFrame].E6AXIS.A2*3.14/180.0;
				axisRotation[2] = playbackHistory[curFrame].E6AXIS.A3*3.14/180.0;
				axisRotation[3] = playbackHistory[curFrame].E6AXIS.A4*3.14/180.0;
				axisRotation[4] = playbackHistory[curFrame].E6AXIS.A5*3.14/180.0;
				axisRotation[5] = playbackHistory[curFrame].E6AXIS.A6*3.14/180.0;
				
				curFrame = (curFrame+1)%100;
			}
		});
	} );
	
	window.setTimeout(function(){
		$.ajax({
			method: "GET",
			url: "api.php",
			data: {
				type: "moveset",
				from: 1,
				to: 2
			}
		}).done(function(response) {
			playbackHistory = eval(response);
			window.setInterval(function(){
				if(playing){
					setAxisRotation(playbackHistory[curFrame].E6AXIS.A1, playbackHistory[curFrame].E6AXIS.A2, playbackHistory[curFrame].E6AXIS.A3, playbackHistory[curFrame].E6AXIS.A4, playbackHistory[curFrame].E6AXIS.A5, playbackHistory[curFrame].E6AXIS.A6);
					curFrame = (curFrame+1)%100;
				}
				$("#slider").slider('value',curFrame);
			}, 1000);
		});
	}, 1000);
});

