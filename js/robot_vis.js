function setAxisRotation(ax1, ax2, ax3, ax4, ax5, ax6){
	axisRotationTarget[0] = ax1*3.1415/180;
	axisRotationTarget[1] = ax2*3.1415/180;
	axisRotationTarget[2] = ax3*3.1415/180;
	axisRotationTarget[3] = ax4*3.1415/180;
	axisRotationTarget[4] = ax5*3.1415/180;
	axisRotationTarget[5] = ax6*3.1415/180;
}

function setCanvasSize(width, height){
	renderer.setSize( width, height );
	camera.aspect = width/height;
	
	camera.updateProjectionMatrix();
}


var axisRotation = Array(6);
axisRotation[0] = 0;
axisRotation[1] = 0;
axisRotation[2] = 0;
axisRotation[3] = 0;
axisRotation[4] = 0;
axisRotation[5] = 0;

var axisRotationTarget = Array(6);
axisRotationTarget[0] = 0;
axisRotationTarget[1] = 0;
axisRotationTarget[2] = 0;
axisRotationTarget[3] = 0;
axisRotationTarget[4] = 0;
axisRotationTarget[5] = 0;

var robot = Array(6);


var camera, scene, renderer, startTime, object;

function init() {

	camera = new THREE.PerspectiveCamera(
			36, window.innerWidth / window.innerHeight, 0.25, 16 );

	
	camera.position.set( 0, 3.3, 6 );

	scene = new THREE.Scene();

	// Lights

	scene.add( new THREE.AmbientLight( 0x505050 ) );

	var spotLight = new THREE.SpotLight( 0xffffff );
	spotLight.angle = Math.PI / 5;
	spotLight.penumbra = 0.2;
	spotLight.position.set( 2, 3, 4 );
	spotLight.castShadow = true;
	spotLight.shadow.camera.near = 1;
	spotLight.shadow.camera.far = 15;
	spotLight.shadow.mapSize.width = 1024;
	spotLight.shadow.mapSize.height = 1024;
	scene.add( spotLight );

	var dirLight = new THREE.DirectionalLight( 0x55505a, 1 );
	dirLight.position.set( 0, 3, 0 );
	dirLight.castShadow = true;
	dirLight.shadow.camera.near = 1;
	dirLight.shadow.camera.far = 15;

	dirLight.shadow.camera.right = 1;
	dirLight.shadow.camera.left = - 1;
	dirLight.shadow.camera.top	= 1;
	dirLight.shadow.camera.bottom = - 1;

	dirLight.shadow.mapSize.width = 1024;
	dirLight.shadow.mapSize.height = 1024;
	scene.add( dirLight );


	var localPlane = new THREE.Plane( new THREE.Vector3( 0, - 1, 0 ), 0.8 ),
		globalPlane = new THREE.Plane( new THREE.Vector3( - 1, 0, 0 ), 0.1 );

	// Loader
	// model

	var manager = new THREE.LoadingManager();
	manager.onProgress = function ( item, loaded, total ) {

		console.log( item, loaded, total );

	};
	
	
	
	var kukaMaterial = new THREE.MeshPhongMaterial( {
			color: 0xFF7300,
			shininess: 100,
			side: THREE.DoubleSide,
			clipShadows: true

		} );
	
	var loader = new THREE.txtLoader( manager );
	loadMesh(loader, 'base.txt', kukaMaterial);
	loadMesh(loader, 'second.txt', kukaMaterial);
	loadMesh(loader, 'third.txt', kukaMaterial);
	loadMesh(loader, 'forth.txt', kukaMaterial);
	loadMesh(loader, 'fifth.txt', kukaMaterial);
	loadMesh(loader, 'sixth.txt', kukaMaterial);
	
		
	// Geometry

	var ground = new THREE.Mesh(
			new THREE.PlaneBufferGeometry( 9, 9, 1, 1 ),
			new THREE.MeshPhongMaterial( {
				color: 0xa0adaf, shininess: 150 } ) );

	ground.rotation.x = - Math.PI / 2; // rotates X/Y to X/Z
	ground.receiveShadow = true;
	scene.add( ground );

	// Renderer

	renderer = new THREE.WebGLRenderer();
	renderer.shadowMap.enabled = true;
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	window.addEventListener( 'resize', onWindowResize, false );
	document.getElementById('renderingCanvas').appendChild( renderer.domElement );
	setCanvasSize(document.getElementById("main").clientWidth, 200);
			

	// Controls

	var controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.target.set( 0, 1, 0 );
	controls.update();

	// GUI

	// Start

	startTime = Date.now();

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

	var currentTime = Date.now(),
		time = ( currentTime - startTime ) / 1000;

		for(var i = 0; i<6; i++){
			if(axisRotation[i]-axisRotationTarget[i]<-0.01){
				axisRotation[i] += 0.01;
			}else if(-axisRotation[i]+axisRotationTarget[i]<-0.01){
				axisRotation[i] -= 0.01;
			}else{
				axisRotation[i] = axisRotationTarget[i];
			}
			axisRotation[i] = (axisRotation[i]*20+axisRotationTarget[i])/21;
		}
		
		
		/*
		axisRotation[0] += 0.02;
		axisRotation[1] = Math.sin(currentTime/1000.0)*0.7;
		axisRotation[2] = Math.sin(currentTime/1131.0)*0.7;
		axisRotation[3] -= 0.01;
		axisRotation[5] = Math.sin(currentTime/532.0)*0.7;
		*/
		
	if(robot["second.txt"]!=undefined){
		robot["second.txt"].position.y = 0.2;
		robot["second.txt"].rotation.y = axisRotation[0];
	}
	
	if(robot["third.txt"]!=undefined){
		robot["third.txt"].position.y = 0.65;
		robot["third.txt"].position.x = 0.35*Math.cos(axisRotation[0]);
		robot["third.txt"].position.z = -0.35*Math.sin(axisRotation[0]);
		
		
		robot["third.txt"].rotation.y = axisRotation[0];
		robot["third.txt"].rotation.z = axisRotation[1];
	}
	
	if(robot["third.txt"]!=undefined && robot["forth.txt"]!=undefined){
		robot["forth.txt"].position.y = 0.65+1.35*Math.cos(axisRotation[1]);
		robot["forth.txt"].position.x = robot["third.txt"].position.x-1.35*Math.sin(axisRotation[1])*Math.cos(axisRotation[0]);
		robot["forth.txt"].position.z = robot["third.txt"].position.z+1.35*Math.sin(axisRotation[1])*Math.sin(axisRotation[0]);
		
		robot["forth.txt"].rotation.y = axisRotation[0];
		robot["forth.txt"].rotation.z = axisRotation[1]+axisRotation[2];
	}
	
	if(robot["third.txt"]!=undefined && robot["forth.txt"]!=undefined && robot["sixth.txt"]!=undefined){
		robot["fifth.txt"].position.y = -0.03+robot["forth.txt"].position.y+1.15*Math.sin(axisRotation[1]+axisRotation[2]);
		robot["fifth.txt"].position.x = robot["forth.txt"].position.x+1.15*Math.cos(axisRotation[1]+axisRotation[2])*Math.cos(axisRotation[0]);
		robot["fifth.txt"].position.z = robot["forth.txt"].position.z-1.15*Math.cos(axisRotation[1]+axisRotation[2])*Math.sin(axisRotation[0]);
		robot["fifth.txt"].rotation.y = axisRotation[0];
		robot["fifth.txt"].rotation.z = axisRotation[1]+axisRotation[2];
		
		robot["sixth.txt"].position.y = -0.03+robot["forth.txt"].position.y+1.35*Math.sin(axisRotation[1]+axisRotation[2]);
		robot["sixth.txt"].position.x = robot["forth.txt"].position.x+1.35*Math.cos(axisRotation[1]+axisRotation[2])*Math.cos(axisRotation[0]);
		robot["sixth.txt"].position.z = robot["forth.txt"].position.z-1.35*Math.cos(axisRotation[1]+axisRotation[2])*Math.sin(axisRotation[0]);
		
		robot["sixth.txt"].rotation.y = axisRotation[0];
		robot["sixth.txt"].rotation.z = axisRotation[1]+axisRotation[2]+axisRotation[5];
	}
		
	requestAnimationFrame( animate );

	renderer.render( scene, camera );

}

function loadMesh(loader, mesh, material){
	var onProgress = function ( xhr ) {
		if ( xhr.lengthComputable ) {
			var percentComplete = xhr.loaded / xhr.total * 100;
			console.log( Math.round(percentComplete, 2) + '% downloaded' );
		}
	};

	var onError = function ( xhr ) {
	};
	

	loader.load( "meshs/"+mesh, function ( object ) {

		object.traverse( function ( child ) {

			if ( child instanceof THREE.Mesh ) {
				child.material = material;
				child.castShadow = true;
			}

		} );
		object.castShadow = true;
		scene.add( object );
		
		robot[mesh] = object;

	}, onProgress, onError );
}

window.onload = function(){
init();
animate();
};
